Commerce CardSave
=================

Introduction
------------

Commerce CardSave is Drupal Commerce module that integrates
the CardSave (http://cardsave.net) payement gateway into your Drupal
Commerce store.

Features
--------

1. Multiple currencies support.
2. Pre-authorization and capture — thus avoiding refund charges
for you as a merchant in the case of a return by a customer, also
allowing complete control of order balancing.
3. Card on file functionality that allows for you securely to
charge a client card without having to deal with the huge hassle of
storing credit card numbers.
4. Complete or partial order refund after funds collection. 

Installation
------------

1. Download and enable the module.
3. Get a merchant account at 
https://mms.cardsaveonlinepayments.com/Pages/PublicPages/RegisterMerchant.aspx
and configure the payment rule at "admin/commerce/config/payment-methods".
4. Configure the payment rule (the "edit" link) with your keys.
5. Done.
